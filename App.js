import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  ScrollView,
  FlatList,
  StyleSheet
} from 'react-native';

import Post, { PostService } from './src/components/Post';
import Feed from './src/components/Feed';


const screenWidth = Dimensions.get('screen').width;

// Trazendo os dados mockados
const postService = new PostService();
const users = postService.state.users;

const App: () => React$Node = () => {
  return (
    <>
      <View>
        <Feed />
      </View>      
    </>
  );
};

export default App;
