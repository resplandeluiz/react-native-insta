import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  ScrollView,
  FlatList,
  StyleSheet
} from 'react-native';

import Post, { PostService } from './Post';


const screenWidth = Dimensions.get('screen').width;

// Trazendo os dados mockados
const postService = new PostService();
const users = postService.state.users;

 class Feed extends Component {

  constructor() {
    super();
    this.state = { users: users };
  }

  addComment(userId,commentValue, inputComment) {

    if (commentValue === '')
      return;

    const user = this.state.users.find(user => user.id === userId)

    const newList = [...user.comments, {
      id: commentValue,
      user: 'Luiz Resplande',
      comment: commentValue
    }];

    const userAtualizado = {
      ...user,
      comments: newList
    }

    const users = this.state.users.map(user=> user.id === userAtualizado.id ? userAtualizado : user);

    this.setState({ users });
    inputComment.clear();
  }

  like(userId) {

    const user = this.state.users.find(user => user.id == userId);

    let newList = [];
    if (!user.likeable) {
      newList = [
        ...user.likers,
        { login: 'myUser' }
      ]
    } else {
      newList = user.likers.filter(liker => {
        return liker.login !== 'myUser';
      });
    }
    const userAtualizado = {
      ...user,
      likeable: !user.likeable,
      likers: newList
    }

    const users = this.state.users.map(user => user.id === userAtualizado.id ? userAtualizado : user);
    this.setState({ users: users });
  }


  render() {
    return (
      <>
        <FlatList keyExtractor={item => item.id}
          data={this.state.users} renderItem={({ item }) =>
            <Post user={item} likeCallback={this.like.bind(this)} commentCallback={this.addComment.bind(this)} />} />
      </>
    )
  }


}

export default Feed;