
import React, { Component } from 'react';
import {
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    TextInput,
    StyleSheet
} from 'react-native';

const screenWidth = Dimensions.get('screen').width;

export class CommentInput extends Component {

    constructor() {
        super();
        this.state = {
            commentValue: ''
        }
    }

    render() {
        return (
            <>
                <View style={styles.container}>
                    <TextInput style={styles.input} onChangeText={text => this.setState({ commentValue: text })} placeholder="Adicione seu comentário..." ref={input => this.inputComment = input} />
                    <TouchableOpacity onPress={() => {
                        this.props.callbackComment(this.props.userId,this.state.commentValue, this.inputComment),
                            this.setState({ commentValue: '' })
                    }}>
                        <Image style={styles.submitIcon} source={require('../../img/send-button.png')} />
                    </TouchableOpacity>
                </View>
            </>
        );

    }

}

const styles = StyleSheet.create({
    input: { flex: 1, height: 40 },
    submitIcon: { width: 30, height: 30 },
    container: { flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#ddd' }
});