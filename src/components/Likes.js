import React, { Component } from 'react';
import {
    Image,
    TouchableOpacity,
    View,
    StyleSheet,
    Text
} from 'react-native';

export class Likes extends Component {

    carregaIcon(likeable) {
        return likeable ? require('../../img/like.png') : require('../../img/nolike.png');
    }

    showLikers(likers) {
        if (likers.length <= 0)
            return;
        return (<Text style={styles.likeCount}>{likers.length} {likers.length > 1 ? 'curtidas' : 'curtida'} </Text>);
    }

    render() {

        const { user, callbackLike } = this.props;

        return (
            <>
                <View>
                    <TouchableOpacity onPress={() => { callbackLike(user.id) }}>
                        <Image style={styles.likeButton} source={this.carregaIcon(user.likeable)} />
                    </TouchableOpacity>
                    {this.showLikers(user.likers)}
                </View>
            </>
        )
    };

}


const styles = StyleSheet.create({
    likeButton: { width: 30, height: 30, marginBottom: 5 },
    likeCount: { fontWeight: 'bold' },
});

