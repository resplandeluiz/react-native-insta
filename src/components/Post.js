import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { CommentInput } from './CommentInput';
import { Likes } from './Likes';

const screenWidth = Dimensions.get('screen').width;

export class PostService extends Component {

    constructor() {
        super();
        this.state = {
            users: [{
                
                "id": 1,
                "name": "Luiz Resplande",
                "profilePicture": "http://www.hotavatars.com/wp-content/uploads/2019/01/I80W1Q0.png",
                "urlPicture": "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTeNkiJOJ9veK4GU8ou2NpwvyV-fqW-A4ez8tItl3-Y-1CA-x31",
                "likeable": 3,
                "likers": [
                    { id: 1, login: 'myUser' },
                    { id: 2, login: 'Patrícia Fernanda' },
                    { id: 3, login: 'Pedro Henrique' }
                ],
                "legend": 'Estou esperando este jogo!',
                "comments": [
                    { id: 1, user: 'Pedro Henrique', comment: 'Nunca vai lançar este jogo!' },
                    { id: 2, user: 'Patrícia Fernanda', comment: 'Gameplay muito zoado!' }
                ]
            },
            {
                "id": 2,
                "name": "Pedro Henrique",
                "profilePicture": "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRI2DTf9Kxd0inLKvLibvk9GIZF5oov_j0aBOvuljhPR11QVvgK",
                "urlPicture": "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQaAk3wFomuTlOO_olKaws5BeRODyTtNU2bPHmnW5rsyacOZb3P",
                "likeable": 0,
                "likers": [],
                "legend": 'Gumball é demais!',
                "comments": []
            },
            {
                "id": 3,
                "name": "Patrícia Fernanda",
                "profilePicture": "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTUe-En4XHPFO3fXWJpbv0hl5neZxA8lkSdI6Bm6hifvSKRq0Ei",
                "urlPicture": "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSmEKxpixNPqTZ1yV76xnQpMt8xGmvcTBIM0ZF5LDDAjXYBf1yL",
                "likeable": 0,
                "likers": [],
                "legend": 'Harry Potter é muito bom!',
                "comments": []
            }]
        }
    }

    // componentDidMount() {
    //     fetch('http://instalura-api.herokuapp.com/')
    //         .then(resposta => resposta.json())
    //         .then(json => this.setState({ users: json }));
    // }


}


export default class Post extends Component {

    showlegend(user) {
        if (user.comments === '')
            return;

        return (<View style={styles.legendBox}><Text style={styles.legendTitle}>{user.name}</Text><Text>{user.legend}</Text></View>);
    }



    render() {

        const { user, likeCallback, commentCallback } = this.props;

        return (
            <>
                <View>
                    <View style={styles.item}>
                        <Image source={{ uri: user.profilePicture }} style={styles.itemImage} />
                        <Text> {user.name}</Text>
                    </View>
                    <Image source={{ uri: user.urlPicture }} style={styles.imagePost} />
                    <View style={styles.imageBottom}>

                        <Likes user={user} callbackLike={likeCallback} />

                        {this.showlegend(user)}

                        {user.comments.map(comment =>
                            <View key={comment.id} style={styles.legendBox}>
                                <Text style={styles.legendTitle}>{comment.user}</Text>
                                <Text>{comment.comment}</Text>
                            </View>
                        )}

                        <CommentInput userId={user.id} callbackComment={commentCallback} />

                    </View>
                </View>
            </>
        );
    }
};

const styles = StyleSheet.create({

    item: { margin: 10, flexDirection: 'row', alignItems: 'center' },
    itemImage: { width: 40, height: 40, borderRadius: 20 },
    imagePost: { width: screenWidth, height: screenWidth },
    imageBottom: { margin: 10 },
    legendBox: { flexDirection: 'row' },
    legendTitle: { fontWeight: 'bold', marginRight: 5 }

});


