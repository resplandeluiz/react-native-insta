import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    Button,
    AsyncStorage
} from 'react-native';
import { Header } from 'react-native/Libraries/NewAppScreen';

const screenWidth = Dimensions.get('screen').width;

export class Login extends Component {

    constructor() {
        super();
        this.state = {
            userName: '',
            userPassword: '',
            mensagem: ''
        }
    }

    makeLogin() {

        const uri = 'http://instalura-api.herokuapp.com/api/public/login';
        const requestInfo = {
            method: 'POST',
            body: JSON.stringify({
                login: this.state.userName,
                senha: this.state.userPassword
            }),
            headers: new Headers({
                'Content-type': 'application/json'
            })
        };

        fetch(uri, requestInfo).then(response => {
            if (response.ok)
                return response.text();
            throw new Error(`Não foi possível realizar o login com o usuario: ${this.state.userName}`);
        }).then(token => {
            AsyncStorage.setItem('token', token);
            AsyncStorage.setItem('usuario', this.state.userName);
        }).catch(e => this.setState({ mensagem: e.message }));

    }

    render() {
        return (
            <>
                <View style={styles.container}>
                    <Text style={styles.tittle}>Insta</Text>
                    <View style={styles.form}>
                        <TextInput autoCapitalize='none' style={styles.input} placeholder="User name" onChangeText={text => this.setState({ userName: text })} />
                        <TextInput secureTextEntry={true} style={styles.input} placeholder="User password" onChangeText={password => this.setState({ userPassword: password })} />
                        <Button title="Login" onPress={this.makeLogin.bind(this)} />
                    </View>

                    <Text style={styles.message}>
                        {this.state.mensagem}
                    </Text>

                </View>
            </>
        )
    };

}

const styles = StyleSheet.create({

    container: { alignItems: 'center', justifyContent: 'center', flex: 1 },
    form: { width: screenWidth * 0.8 },
    input: { height: 40, borderBottomWidth: 1, borderBottomColor: '#ddd', marginBottom: 10 },
    tittle: { fontWeight: 'bold', fontSize: 20 },
    message: { marginTop: 20, color: 'red' }

});